/*  PX4Flow Driver for 
 *  Beaglebone Black Hardware Interface Function Library
 *  For Use in Robotics 550, University of Michigan
 *
 *  References:  
 *      http://beagleboard.org/
 *
 * 
 *  Based on code (c)2014 by
 *  Laurent Eschenauer <laurent@eschenauer.be>
 * 
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <lcm/lcm.h>

#define EXTERN extern
#include "../bbblib/bbb.h"
#include "PX4Flow.h"
#include "../include/util.h"
#include "../lcmtypes/px4flow_t.h"
#include "../lcmtypes/px4flow_basic_t.h"
#include "../lcmtypes/px4flow_integral_t.h"

struct I2C_data px4flow;

// 'a' for px4flow_t, 'b' for px4flow_basic_t, 'i' for px4flow_integral_t
int initialize(char lcm_type){
  if(bbb_init() == -1){
    printf("Error initializing BBB.\n");
    return -1;
  }

  px4flow.name = I2C_1;
  px4flow.address = (PX4FLOW_ADDRESS << 1);
  px4flow.flags = O_RDWR;

  if(bbb_initI2C(&px4flow) == -1){
    printf("Error initializing I2C port for PX4Flow.\n");
    return -1;
  }

  lcm_t* lcm = lcm_create(NULL);
  px4flow_t msg;
  px4flow_basic_t bmsg;
  px4flow_integral_t imsg;

  while(1){
    if(lcm_type == 'b'){
      update();

      bmsg.utime = utime_now();

      bmsg.frame_count = frame.frame_count;
      bmsg.pixel_flow_x_sum = frame.pixel_flow_x_sum;
      bmsg.pixel_flow_y_sum = frame.pixel_flow_y_sum;
      bmsg.flow_comp_m_x = frame.flow_comp_m_x;
      bmsg.flow_comp_m_y = frame.flow_comp_m_y;
      bmsg.qual = frame.qual;
      bmsg.gyro_x_rate = frame.gyro_x_rate;
      bmsg.gyro_y_rate = frame.gyro_y_rate;
      bmsg.gyro_z_rate = frame.gyro_z_rate;
      bmsg.gyro_range = frame.gyro_range;
      bmsg.sonar_timestamp = frame.sonar_timestamp;
      bmsg.ground_distance = frame.ground_distance;

      px4flow_basic_t_publish(lcm, "BPX4FLOW", &bmsg);
    }
    else if(lcm_type == 'i'){
      update_integral();

      imsg.utime = utime_now();

      imsg.frame_count_since_last_readout = iframe.frame_count_since_last_readout;
      imsg.pixel_flow_x_integral = iframe.pixel_flow_x_integral;
      imsg.pixel_flow_y_integral = iframe.pixel_flow_y_integral;
      imsg.gyro_x_rate_integral = iframe.gyro_x_rate_integral;
      imsg.gyro_y_rate_integral = iframe.gyro_y_rate_integral;
      imsg.gyro_z_rate_integral = iframe.gyro_z_rate_integral;
      imsg.integration_timespan = iframe.integration_timespan;
      imsg.sonar_timestamp = iframe.sonar_timestamp;
      imsg.ground_distance = iframe.ground_distance;
      imsg.gyro_temperature = iframe.gyro_temperature;
      imsg.quality = iframe.quality;

      px4flow_integral_t_publish(lcm, "IPX4FLOW", &imsg);
    }
    else{
      update();

      msg.utime = utime_now();

      msg.frame_count = frame.frame_count;
      msg.pixel_flow_x_sum = frame.pixel_flow_x_sum;
      msg.pixel_flow_y_sum = frame.pixel_flow_y_sum;
      msg.flow_comp_m_x = frame.flow_comp_m_x;
      msg.flow_comp_m_y = frame.flow_comp_m_y;
      msg.qual = frame.qual;
      msg.gyro_x_rate = frame.gyro_x_rate;
      msg.gyro_y_rate = frame.gyro_y_rate;
      msg.gyro_z_rate = frame.gyro_z_rate;
      msg.gyro_range = frame.gyro_range;
      msg.sonar_timestamp = frame.sonar_timestamp;
      msg.ground_distance = frame.ground_distance;

      update_integral();

      msg.frame_count_since_last_readout = iframe.frame_count_since_last_readout;
      msg.pixel_flow_x_integral = iframe.pixel_flow_x_integral;
      msg.pixel_flow_y_integral = iframe.pixel_flow_y_integral;
      msg.gyro_x_rate_integral = iframe.gyro_x_rate_integral;
      msg.gyro_y_rate_integral = iframe.gyro_y_rate_integral;
      msg.gyro_z_rate_integral = iframe.gyro_z_rate_integral;
      msg.integration_timespan = iframe.integration_timespan;
      msg.integral_sonar_timestamp = iframe.sonar_timestamp;
      msg.integral_ground_distance = iframe.ground_distance;
      msg.gyro_temperature = iframe.gyro_temperature;
      msg.quality = iframe.quality;

      px4flow_t_publish(lcm, "PX4FLOW", &msg);
    }

  }

  bbb_deinitI2C(&px4flow);
  return 0;
}

void update(){

  //send 0x0 to PX4FLOW module and receive back 22 Bytes data 
  byte buf, buffer[22];
  buf = 0x0;
  bbb_writeI2C(&px4flow,&buf,sizeof(buf));
  bbb_readI2C(&px4flow,buffer,sizeof(buffer[0])*22);

  frame.frame_count       = buffer[0] + (uint16_t)(buffer[1] << 8);
  frame.pixel_flow_x_sum  = buffer[2] + (uint16_t)(buffer[3] << 8);
  frame.pixel_flow_y_sum  = buffer[4] + (uint16_t)(buffer[5] << 8);
  frame.flow_comp_m_x     = buffer[6] + (uint16_t)(buffer[7] << 8);
  frame.flow_comp_m_y     = buffer[8] + (uint16_t)(buffer[9] << 8);
  frame.qual              = buffer[10] + (uint16_t)(buffer[11] << 8);
  frame.gyro_x_rate       = buffer[12] + (uint16_t)(buffer[13] << 8);
  frame.gyro_y_rate       = buffer[14] + (uint16_t)(buffer[15] << 8);
  frame.gyro_z_rate       = buffer[16] + (uint16_t)(buffer[17] << 8);
  frame.gyro_range        = (uint8_t)(buffer[18]);
  frame.sonar_timestamp   = (uint8_t)(buffer[19]);
  frame.ground_distance   = buffer[20] + (uint16_t)(buffer[21] << 8);
}

void update_integral(){

  //send 0x16 to PX4FLOW module and receive back 25 Bytes data 
  byte buf, buffer[26];
  buf = 0x16;
  bbb_writeI2C(&px4flow,&buf,sizeof(buf));
  bbb_readI2C(&px4flow,buffer,sizeof(buffer[0])*26);
  
  iframe.frame_count_since_last_readout = buffer[0] + (uint16_t)(buffer[1] << 8);
  iframe.pixel_flow_x_integral  = buffer[2] + (uint16_t)(buffer[3] << 8);
  iframe.pixel_flow_y_integral  = buffer[4] + (uint16_t)(buffer[5] << 8);
  iframe.gyro_x_rate_integral   = buffer[6] + (uint16_t)(buffer[7] << 8);
  iframe.gyro_y_rate_integral   = buffer[8] + (uint16_t)(buffer[9] << 8);
  iframe.gyro_z_rate_integral   = buffer[10] + (uint16_t)(buffer[11] << 8);
  iframe.integration_timespan   = (uint32_t)(buffer[12] + (uint16_t)(buffer[13] << 8))
                           + (uint32_t)((buffer[14] + (uint16_t)(buffer[15] << 8)) << 16);
  iframe.sonar_timestamp        = (uint32_t)(buffer[16] + (uint16_t)(buffer[17] << 8))
                           + (uint32_t)((buffer[18] + (uint16_t)(buffer[19] << 8)) << 16);
  iframe.ground_distance        = buffer[20] + (uint16_t)(buffer[21] << 8);
  iframe.gyro_temperature       = buffer[22] + (uint16_t)(buffer[23] << 8);
  iframe.quality                = (uint8_t)(buffer[24]);

}

// Simple frame
uint16_t frame_count() {
  return frame.frame_count;
}

int16_t pixel_flow_x_sum() {
  return frame.pixel_flow_x_sum;
}

int16_t pixel_flow_y_sum() {
  return frame.pixel_flow_y_sum;
}

int16_t flow_comp_m_x() {
  return frame.flow_comp_m_x;
}

int16_t flow_comp_m_y() {
  return frame.flow_comp_m_y;
}

int16_t gyro_x_rate() {
  return frame.gyro_x_rate;
}

int16_t gyro_y_rate() {
  return frame.gyro_y_rate;
}

int16_t gyro_z_rate() {
  return frame.gyro_z_rate;
}

int16_t qual() {
  return frame.qual;
}

uint8_t sonar_timestamp() {
  return frame.sonar_timestamp;
}

int16_t ground_distance() {
  return frame.ground_distance;
}

// Integral frame
uint16_t frame_count_since_last_readout() {
  return iframe.frame_count_since_last_readout;
}

int16_t pixel_flow_x_integral() {
  return iframe.pixel_flow_x_integral;
}

int16_t pixel_flow_y_integral() {
  return iframe.pixel_flow_y_integral;
}

int16_t gyro_x_rate_integral() {
  return iframe.gyro_x_rate_integral;
}

int16_t gyro_y_rate_integral() {
  return iframe.gyro_y_rate_integral;
}

int16_t gyro_z_rate_integral() {
  return iframe.gyro_z_rate_integral;
}

uint32_t integration_timespan() {
  return iframe.integration_timespan;
}

uint32_t sonar_timestamp_integral() {
  return iframe.sonar_timestamp;
}
      
int16_t ground_distance_integral() {
  return iframe.ground_distance;
}

int16_t gyro_temperature() {
  return iframe.gyro_temperature;
}

uint8_t quality_integral() {
  return iframe.quality;
}

