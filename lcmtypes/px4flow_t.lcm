struct px4flow_t
{
  int64_t utime; // from util.c

  int16_t frame_count; // counts created I2C frames [# frames]
  int16_t pixel_flow_x_sum; // latest x flow measurements in pixels*10 [pixels]
  int16_t pixel_flow_y_sum; // latest y flow measurements in pixels*10 [pixels]
  int16_t flow_comp_m_x; // x velocity*1000 [meters/sec]
  int16_t flow_comp_m_y; // y velocity*1000 [meters/sec]
  int16_t qual; // optical flow quality/confidence [0: bad, 255: maximum quality]
  int16_t gyro_x_rate; // latest gyro x rate [rad/sec]
  int16_t gyro_y_rate; // latest gyro y rate [rad/sec]
  int16_t gyro_z_rate; // latest gyro z rate [rad/sec]
  int8_t gyro_range; // gyro range [0 ... 7] equals [50 deg/s ... 2000 deg/s]
  int8_t sonar_timestamp; // time since last sonar update [milliseconds]
  int16_t ground_distance; // ground distance in meters*1000 [meters] >0: distance known, <0 distance unknown

  int16_t frame_count_since_last_readout; // # of flow measurements since last I2C readout [# frames]
  int16_t pixel_flow_x_integral; // accumulated flow in radians*10000 around x axis since last I2C readout [rad*10000]
  int16_t pixel_flow_y_integral; // accumulated flow in radians*10000 around y axis since last I2C readout [rad*10000]
  int16_t gyro_x_rate_integral; // accumulated gyro x rates in radians*10000 around x axis since last I2C readout [rad*10000]
  int16_t gyro_y_rate_integral; // accumulated gyro y rates in radians*10000 around x axis since last I2C readout [rad*10000]
  int16_t gyro_z_rate_integral; // accumulated gyro z rates in radians*10000 around x axis since last I2C readout [rad*10000]
  int32_t integration_timespan; // accumulated timespan in microseconds since last I2C readout [microseconds]
  int32_t integral_sonar_timestamp; // time since last sonar update [microseconds]
  int16_t integral_ground_distance; // ground distance in meters*1000 [meters*1000]
  int16_t gyro_temperature; // temperature*100 in centi-degrees Celsius [degcelsius*100]
  int8_t quality; // averaged quality of accumulated flow values [0: bad quality, 255: max quality]
}
