/*
   driver for actuateing servose "smoothly" on the BBB
*/

#define EXTERN  // Needed for global data declarations in bbb.h
#include <unistd.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include "bbblib/bbb.h"
#include "util.h"


#include <linux/spi/spidev.h>
#include <sys/ioctl.h>


#include <lcm/lcm.h>
#include "lcmtypes/servo_setpoint_t.h"
#include "lcmtypes/servo_fb_t.h"

#define BBB 1
#define DEBUG 1
#define STOW_ANG 0.0    // determined by expierementation do not change
//#define STOW_ANG -20.0    // determined by expierementation do not change

typedef struct servoData {
    double angleSetpoint;
    double angleNow;
    double rateNow;
} servoData_t;

servoData_t servoData[4];
double masterRate;
const int servoMap[4] = {0, 3, 1, 2};

double angleToDuty(double angle);
int initServos(void);
int initAll(void);

void servo_setpoint_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const servo_setpoint_t *msg, void *userdata) {
    for(int i = 0; i<4; i++) {
        servoData[i].angleSetpoint = msg->angle[i];
    }
    masterRate = msg->rate;
    printf("Received message on channel %s, timestamp %lld\n",
           channel, msg->utime);
    for (int i = 0; i < 4; i += 1) {
        printf("\t%d: angle = % 8.3lf, rate = % 8.3lf\n", 
                i, msg->angle[i], msg->rate);
    }
    printf("\n");
}
int main(int argc, char *argv[]) {
    const char *channel = "SERVO_SETPOINT";

#if BBB 
    initAll();
#endif
    // initialize state data
    masterRate = 5;
    
    servoData[0].angleSetpoint = 90.0 - STOW_ANG;
    servoData[0].angleNow = 90.0 - STOW_ANG;
    servoData[0].rateNow = 0.0; // not used

    servoData[1].angleSetpoint = 90.0 - STOW_ANG;
    servoData[1].angleNow = 90.0 + STOW_ANG;
    servoData[1].rateNow = 0.0; // not used

    servoData[2].angleSetpoint = 90.0 - STOW_ANG;
    servoData[2].angleNow = 90.0 - STOW_ANG;
    servoData[2].rateNow = 0.0; // not used

    servoData[3].angleSetpoint = 90.0;
    servoData[3].angleNow = 90.0;
    servoData[3].rateNow = 0.0;

    // Initialize LCM
    lcm_t *lcm = lcm_create(NULL);
    servo_setpoint_t_subscribe(lcm, channel, servo_setpoint_handler, NULL);

    int updateServos = 1;
    for(;;) {
        lcm_handle_timeout(lcm, 10);    // timout ensures fast updates to servos

        // send message back for current servo posiiton feedback
        // Initialize LCM
        servo_fb_t fb = {};
        
        //TODO do this a smarter way, like cubic spline
        double stepSize = masterRate*1;
        for(int i=0; i<4; i++) {
            double angleNow = servoData[i].angleNow;
            double setPoint = servoData[i].angleSetpoint;
            double angError = setPoint - angleNow;
            fb.angle_error[i] = angError;
            if(fabs(angError) <= stepSize) {
                if(fabs(angError) > 0.0) updateServos = 1;
                servoData[i].angleNow  = setPoint;
            } else {
                updateServos = 1;
                if(angError < 0)
                    servoData[i].angleNow -= stepSize;
                if(angError > 0)
                    servoData[i].angleNow += stepSize;
                if(i == 3)
                    servoData[i].angleNow = setPoint;
            }
            //servoData[i].angleNow = angleNow;
        }

        if(updateServos) {

#if BBB
            for(int i=0; i<4; i++) {
                int idx = servoMap[i];
                bbb_setDutyPWM(idx, angleToDuty(servoData[i].angleNow));
            }
#endif
#if DEBUG
            printf("Servo angles (% 8.3lf,% 8.3lf,% 8.3lf,% 8.3lf)\n",
                    servoData[0].angleNow,
                    servoData[1].angleNow,
                    servoData[2].angleNow,
                    servoData[3].angleNow
                    );
#endif
        }
        updateServos = 0;

        fb.utime = utime_now();
        servo_fb_t_publish(lcm, "SERVO_FB", &fb);
        
    }
    lcm_destroy(lcm);
    return 0;
}
double map(double x, double in_min, double in_max,
            double out_min, double out_max) {   
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;   
}
double angleToDuty(double angle) {
    angle = angle <   0.0 ?   0.0 :
            angle > 180.0 ? 180.0 : angle;
    return map(angle, 0.0, 180.0, 2.5, 13.0);
}
int initAll(void) { // TODO move this to the servo driver
    if (bbb_init()) {
        printf("Error initializing BBB.\n");
        return -1;
    }
    if(initServos()) {
        printf("Error initializing BBB Servo pins. Are you running as root?\n");
        return -1;
    }
    return 0;
}
int initServos(void) {
    for(int i=0; i<4; i++) {
        if(bbb_initPWM(       servoMap[i]))            return -1;
        if(bbb_setPeriodPWM(  servoMap[i],  20000000)) return -1;   // 50Hz
        if(bbb_setRunStatePWM(servoMap[i],  pwm_run))  return -1;
    }
    //TODO use LCM servo driver to do this
    //bbb_setDutyPWM(0, angleToDuty(90.0));  // servo angle (0-180)
   // bbb_setDutyPWM(1, angleToDuty(90.0));  // servo angle (0-180)
   // bbb_setDutyPWM(2, angleToDuty(90.0));  // servo angle (0-180)
    //bbb_setDutyPWM(3, angleToDuty(90.0));  // servo angle (0-180)
    return 0;
}
