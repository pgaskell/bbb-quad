/** \file
   SPI driver for the CMUcam5 Pixy on Beaglebone Black.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include <lcm/lcm.h>

#include "../lcmtypes/pixy_frame_t.h"
#include "../include/util.h"


int debug = 0;
uint8_t mode = 0; //SPI_NO_CS; - this causes an error when trying to set mode
uint8_t bits = 8;           // bits per data word
uint32_t speed = 1000000;   // in Hz


/** Perform a bidirectional SPI transfer of len bytes. The bytes in txbuf are
    sent to the slave device while the bytes received are placed in rxbuf */
static void spi_transfer(int fd, uint8_t *txbuf, uint8_t *rxbuf, int len)
{
    struct spi_ioc_transfer tfer = {
        .tx_buf = (uintptr_t)txbuf,
        .rx_buf = (uintptr_t)rxbuf,
        .len = len,
        .delay_usecs = 0,
        .speed_hz = speed,
        .bits_per_word = bits,
    };

    int ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tfer);
    if (ret < 1) {
        perror("spi transfer failed");
        abort();
    }
}

/** Read a 16-bit int in big-endian byte order */
static uint16_t readu16(int fd)
{
    uint8_t rx[2];
    spi_transfer(fd, NULL, rx, 2);
    return (rx[1] | rx[0] << 8);
}

/** Read a 8-bit int */
static uint8_t readu8(int fd)
{
    uint8_t rx[1];
    spi_transfer(fd, NULL, rx, 1);
    return rx[0];
}

int main(int argc, char *argv[])
{
    // Parse command line options
    const char *channel = "PIXY";   // LCM channel to publish messages
    int opt;
    while ((opt = getopt(argc, argv, "c:dh")) != -1) {
        switch (opt) {
        case 'c':
            channel = optarg;
            break;

        case 'd':
            printf("Debug messages on\n");
            debug = 1;
            break;

        case 'h':
        case '?':
            printf("Usage: pixy_driver [options] [/path/to/spidev]\n");
            printf("Options:\n");
            printf(" -c LCM_CHANNEL\tPublish to specified LCM channel name\n");
            printf(" -d\t\tTurn on debug messages\n");
            printf(" -h\t\tPrint this help and exit\n");
            exit(1);

        default:
            abort();
        }
    }

    const char *port = "/dev/spidev1.0";
    if (argc > optind)
        port = argv[optind];

    // Open SPI port and set mode
    printf("Opening device %s\n", port);
    int fd = open(port, O_RDWR);
    if (fd == -1) {
        perror("can't open SPI device");
        abort();
    }

    if (ioctl(fd, SPI_IOC_WR_MODE, &mode) == -1)
        perror("can't set spi mode");
    if (ioctl(fd, SPI_IOC_RD_MODE, &mode) == -1)
        perror("can't read spi mode");

    if (ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits) == -1)
        perror("can't set bits per word");
    if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits) == -1)
        perror("can't read bits per word");

    if (ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed) == -1)
        perror("can't set max speed");
    if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed) == -1)
        perror("can't read max speed");

    printf("SPI config: mode %d, %d bits per word, %d Hz max\n",
           mode, bits, speed);

    // Initialize LCM
    lcm_t *lcm = lcm_create(NULL);

    pixy_frame_t msg = {};
    int capacity = 8;
    msg.objects = malloc(capacity * sizeof(pixy_t));

    // Enter read loop
    int valid_blocks = 0;
    int checksum_errors = 0;

    while (1) {
        int i;

        // IMPORTANT: data appears to be big-endian, despite the documentation
        // Wait for sync pattern 0xaa55
        while ((i = readu8(fd)) != 0xaa) {
            usleep(2000);
        }
        if ((i = readu8(fd)) == 0x55) {
            // Read blocks until zero byte is encountered
            // Blocks will begin with pattern 0xaa55 or 0xaa56
            while ((i = readu8(fd)) == 0xaa) {
                if (debug) printf("%02x ", i);

                // Decode block
                pixy_t obj;

                int i = readu8(fd);
                if (debug) printf("%02x ", i);
                if (i == 0x55)
                    obj.type = PIXY_T_TYPE_NORMAL;
                else if (i == 0x56)
                    obj.type = PIXY_T_TYPE_COLOR_CODE;
                else
                    break;

                // Timestamp frame when the first header is is received
                if (msg.nobjects == 0)
                    msg.utime = utime_now();

                int checksum = readu16(fd);
                // Stupid packet format is ambiguous: the previous 0xaa55
                // might be the start of a frame. In that case, checksum
                // is actually the block header (0xaa55 or 0xaa56).
                // Handle this case here:
                if (checksum == 0xaa55 || checksum == 0xaa56) {
                    obj.type = (checksum == 0xaa55) ? PIXY_T_TYPE_NORMAL :
                        PIXY_T_TYPE_COLOR_CODE;

                    // Transmit previous completed frame
                    pixy_frame_t_publish(lcm, channel, &msg);
                    msg.nobjects = 0;
                    msg.utime = utime_now();

                    // Read the real checksum
                    checksum = readu16(fd);
                }

                obj.signature = readu16(fd);
                obj.x = readu16(fd);
                obj.y = readu16(fd);
                obj.width = readu16(fd);
                obj.height = readu16(fd);
                if (obj.type == PIXY_T_TYPE_COLOR_CODE)
                    obj.angle = readu16(fd);
                else
                    obj.angle = 0;

                if (debug)
                    printf("%d %d %d %d %d %d %d\n",
                           checksum, obj.signature,
                           obj.x, obj.y, obj.width, obj.height, obj.angle);

                // Verify checksum
                uint16_t computed_checksum = obj.signature + obj.x + obj.y +
                    obj.width + obj.height + obj.angle;
                if (checksum != computed_checksum) {
                    checksum_errors += 1;
                    if (debug) {
                        printf("failed checksum: received %x, computed %x\n",
                               checksum, computed_checksum);
                    }
                    continue;
                }

                // Valid block received: add to frame
                // Resize msg.objects if we don't have enough space
                if (msg.nobjects >= capacity) {
                    capacity *= 2;
                    pixy_t* new_objects = realloc(msg.objects,
                                                  capacity * sizeof(pixy_t));
                    if (new_objects == NULL) {
                        perror("couldn't realloc array");
                        abort();
                    }
                    msg.objects = new_objects;
                }
                msg.objects[msg.nobjects++] = obj;
                valid_blocks += 1;

                if (debug) {
                    if (obj.type == PIXY_T_TYPE_COLOR_CODE) {
                        printf("Color code %d (octal %o) at (%d, %d) "
                               "size (%d, %d) angle %d\n",
                               obj.signature, obj.signature,
                               obj.x, obj.y, obj.width, obj.height, obj.angle);
                    } else {
                        printf("Signature %d at (%d, %d) size (%d, %d)\n",
                               obj.signature, obj.x, obj.y, obj.width,
                               obj.height);
                    }
                }
            }

            // Finished receiving frame
            pixy_frame_t_publish(lcm, channel, &msg);
            msg.nobjects = 0;
            printf("%d valid blocks read, %d failed checksum\n",
                   valid_blocks, checksum_errors);
        }
    }

    printf("Exiting\n");
    close(fd);
}
