/* Example of receiving LCM messages and processing them in another thread.

   pthread_create(...) starts a thread
   pthread_join(...) waits for a thread to finish
   pthread_mutex is used to prevent the receive thread from modifying the
    shared data when the processing thread is reading it
 */

#include <stdio.h>
#include <inttypes.h>
#include <pthread.h>
#include <unistd.h>
#include <lcm/lcm.h>

#include "../lcmtypes/pixy_frame_t.h"

pthread_mutex_t data_mutex;
int msg_count = 0;

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    printf("Received message on channel %s, timestamp %" PRId64 "\n",
           channel, msg->utime);

    // Do some processing of the pixy data here
    // Caution: don't save the msg pointer; the data won't be valid after
    //  this handler function exists
    printf("Number of objects: %d\n", msg->nobjects);
    for (int i = 0; i < msg->nobjects; i += 1) {
        pixy_t *obj = &msg->objects[i];

        printf("%d: ", i);
        if (obj->type == PIXY_T_TYPE_COLOR_CODE) {
            printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                   " angle %d\n", obj->signature, obj->signature,
                   obj->x, obj->y, obj->width, obj->height, obj->angle);
        } else {
            printf("Signature %d at (%d, %d) size (%d, %d)\n",
                   obj->signature, obj->x, obj->y, obj->width, obj->height);
        }
    }

    // Lock mutex before modifying shared data
    pthread_mutex_lock(&data_mutex);

    // Dumb example of modifying shared data in a global variable
    msg_count += 1;

    pthread_mutex_unlock(&data_mutex);
}

void* lcm_receive_loop(void *data)
{
    lcm_t *lcm = lcm_create(NULL);

    pixy_frame_t_subscribe(lcm, "PIXY", pixy_handler, NULL);

    while (1) {
        lcm_handle(lcm);
    }

    lcm_destroy(lcm);
}

void* processing_loop(void *data)
{
    int hz = 1;

    while (1) {

        // Lock the mutex before reading shared data
        pthread_mutex_lock(&data_mutex);

        printf("msg_count: %d\n", msg_count);

        // We're done -- unlock the mutex
        pthread_mutex_unlock(&data_mutex);

        usleep(1000000/hz);
    }
}

int main()
{
    // Initialize the mutex
    pthread_mutex_init(&data_mutex, NULL);

    // Start the threads
    pthread_t lcm_receive_thread;
    pthread_t processing_thread;
    pthread_create(&lcm_receive_thread, NULL, lcm_receive_loop, NULL);
    pthread_create(&processing_thread, NULL, processing_loop, NULL);

    // Wait for thread to finish (in this case, thread will never finish
    // because it's in an infinite loop)
    pthread_join(lcm_receive_thread, NULL);
}
